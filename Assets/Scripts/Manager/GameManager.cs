﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Sprite[] cardFace;
    public Sprite cardBack;
    public GameObject[] cards;

    int max = 1;
    int chance = 2;
    int start = 1;
    int number = 0;
    public int addNum = 1;

    private bool normalCheck;
    private bool hardCheck;
    public bool switchTurn;
    private bool _init = false;
    public bool chooseCard = true;

    public List<GameObject> cardCheck;
    public List<GameObject> cardIA;
    public List<GameObject> cardIaCheckTrue;

    public GameObject[] button;

    public GameObject menu;
    public GameObject restart;

    public GameObject namePlayer1;
    public GameObject namePlayer2;

    public int victory;

	void Start () {
        switchTurn = false;
        normalCheck = true;
        hardCheck = false;
        victory = 0;
        menu.SetActive(false);
        restart.SetActive(false);
        cardCheck = new List<GameObject>();
        cardIA = new List<GameObject>();
        cardIaCheckTrue = new List<GameObject>();
    }
	
	void Update ()
    {
        if (!_init)
        {
            InitializeCards();
        }

        if (Input.GetMouseButtonUp(0))
        {
            for (int i = 0; i < cards.Length; i++)
            {
                if (cards[i].GetComponent<Cards>().CardClicked == 1)
                {
                    if (cards[i].GetComponent<Cards>().IsClicked == false)
                    {
                        if (!cardCheck.Contains(cards[i]))
                        {
                            cardCheck.Add(cards[i]);
                        }
                        if (!cardIA.Contains(cards[i]))
                        {
                            cardIA.Add(cards[i]);
                        }
                    }
                }
            }
        }
        if (cardCheck.Count == 2)
        {
            Cardcompare(cardCheck);
        }
        for (int b = 0; b < button.Length; b++)
        {
            button[b].SetActive(true);
        }
        Victory(victory);
    }

    public void InitializeCards()
    {
        for(int id = 0; id<2; id++)
        {
            for(int i = 0; i<8; i++)
            {
                bool test = false;
                int valueCard = 0;
                while (!test)
                {
                    valueCard = Random.Range(0, cards.Length);
                    test = !(cards[valueCard].GetComponent<Cards>().Inizialized);
                }
                cards[valueCard].GetComponent<Cards>().CardValue = i;
                cards[valueCard].GetComponent<Cards>().IsClicked = false;
                cards[valueCard].GetComponent<Cards>().Inizialized = true;
            }
        }
        foreach(GameObject c in cards)
        {
            c.GetComponent<Cards>().SetUpGraphic();
        }
        if (!_init)
        {
            _init = true;
        }
    }

    public Sprite GetCardBack()
    {
        return cardBack;
    }

    public Sprite GetCardFace(int numberFace)
    {
        return cardFace[numberFace];
    }

    public void Cardcompare(List<GameObject> card)
    {
        if (card[0].GetComponent<Cards>().CardValue == card[1].GetComponent<Cards>().CardValue)
        {
            victory++;

            for (int i = 0; i<card.Count; i++)
            {
                card[i].GetComponent<Cards>().IsClicked = true;
                if (card[0].GetComponent<Cards>().CardClicked == 1 && card[1].GetComponent<Cards>().CardClicked == 1)
                {
                    switchTurn = true;
                    card[i].GetComponent<Cards>().ControlCheck();
                    cardCheck = new List<GameObject>();
                }
            }
        }

        if(card[0].GetComponent<Cards>().CardValue != card[1].GetComponent<Cards>().CardValue)
        {
            for (int i=0; i< card.Count; i++)
            {
                card[i].GetComponent<Cards>().CardClicked = 0;
                card[i].GetComponent<Cards>().ControlCheck();
                if (card[0].GetComponent<Cards>().CardClicked == 0 && card[1].GetComponent<Cards>().CardClicked == 0)
                {
                    switchTurn = true;
                    card.Clear();
                }
            }
        }
    }

    public void Victory(int i)
    {
        if(i == 8)
        {
            for(int b = 0; b<button.Length; b++)
            {
                button[b].SetActive(false);
            }
            TurnManager.instance.SetState(TurnManager.State.FinishGame);
            menu.SetActive(true);
            restart.SetActive(true);
        }
    }

    public void Menu()
    {
        SceneManager.LoadScene("MainMenu");
    }

    /*public void Restart()
    {
        TurnManager.instance.timerTurn = 15;
        TurnManager.instance.SetState(TurnManager.State.Player1);
        menu.SetActive(false);
        restart.SetActive(false);
    }*/

    public void Easy()
    {
        NormalCheckIa();
    }

    public void Hard()
    {
        if (normalCheck)
        {
            NormalCheckIa();
        }
        else if (hardCheck)
        {
            HardCheckIa();
        }
              
    }

    private void NormalCheckIa()
    {
        if (chooseCard)
        {
            for (int id = 0; id < chance; id++)
            {
                int choose = Random.Range(0, 16);
                if (cards[choose].GetComponent<Cards>().CardClicked == 0 && cards[choose].GetComponent<Cards>().IsClicked == false)
                {
                    cards[choose].GetComponent<Cards>().CardClicked = 1;
                    cards[choose].GetComponent<Cards>().FlipCard();
                    if (cards[choose].GetComponent<Cards>().IsClicked == false)
                    {
                        cardCheck.Add(cards[choose]);
                        if (!cardIA.Contains(cards[choose]))
                        {
                            cardIA.Add(cards[choose]);
                        }
                    }
                }

                if (cards[choose].GetComponent<Cards>().CardClicked == 1 && cards[choose].GetComponent<Cards>().IsClicked == true)
                {
                    chance = chance + 1;
                    max = max + 1;
                }

                if (id == max)
                {
                    max = 1;
                    chance = 2;
                    normalCheck = false;
                    hardCheck = true;
                    chooseCard = false;
                }
            }
        }
    }

    private void HardCheckIa()
    {
        if (chooseCard)
        {
            cardIaCheckTrue.Clear();
            for (int i=number; i<cardIA.Count; i++)
            {
                for(int b = start; b < cardIA.Count; b++)
                {                    
                    if (b == cardIA.Count -1)
                    {
                        start++;
                        if (i == cardIA.Count - 2)
                        {
                            start = 1;
                            number = 0;
                            normalCheck = true;
                            hardCheck = false;
                        }
                    }

                    if (cardIA[i].GetComponent<Cards>().CardValue == cardIA[b].GetComponent<Cards>().CardValue)
                    {
                        if(cardIA[i].GetComponent<Cards>().CardClicked == 0 && cardIA[b].GetComponent<Cards>().CardClicked == 0)
                        {
                            if (cardIA[i].GetComponent<Cards>().IsClicked == false && cardIA[b].GetComponent<Cards>().IsClicked == false)
                            {
                                if (cardIaCheckTrue.Count < 2)
                                {
                                    if (!cardIaCheckTrue.Contains(cardIA[i]))
                                    {
                                        cardIaCheckTrue.Add(cardIA[i]);
                                    }
                                    if (!cardIaCheckTrue.Contains(cardIA[b]))
                                    {
                                        cardIaCheckTrue.Add(cardIA[b]);
                                    }
                                }
                            }
                        }
                    }

                    if(cardIaCheckTrue.Count == 2)
                    {
                        for (int c=0; c<cardIaCheckTrue.Count; c++)
                        {
                            cardIaCheckTrue[c].GetComponent<Cards>().CardClicked = 1;
                            cardIaCheckTrue[c].GetComponent<Cards>().IsClicked = true;
                            if(cardIaCheckTrue[0].GetComponent<Cards>().IsClicked == true && cardIaCheckTrue[1].GetComponent<Cards>().IsClicked == true)
                            {
                                switchTurn = true;
                                if(addNum == 1)
                                {
                                    victory++;
                                    addNum = 0;
                                }
                                cardIaCheckTrue[0].GetComponent<Cards>().ControlCheck();
                                cardIaCheckTrue[1].GetComponent<Cards>().ControlCheck();
                                start = 1;
                                number = 0;
                                chooseCard = false;
                            }
                        }
                    }
                }
            }
        }
    }
}
