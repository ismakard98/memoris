﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurnManager : Singleton<TurnManager>
{

    public bool create;
    private bool turnPlayer1;
    private bool turnPlayer2;

    GameManager manager;
    UiButton uiButton;

    public bool TurnPlayer { get { return turnPlayer1; } set { turnPlayer1 = value; } }
    public bool TurnPlayer2 { get { return turnPlayer2; } set { turnPlayer2 = value; } }

    void Start () {
        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>();
        uiButton = GameObject.FindGameObjectWithTag("Button").GetComponent<UiButton>();
        currentState = State.Player1;
	}

    public enum State
    {
        Player1,
        Player2,
        FinishGame
    }

    public State currentState;

    public void SetState(State state)
    {
        currentState = state;
    }

    void Update () {

        switch (currentState)
        {
            case State.Player1:
                manager.namePlayer2.SetActive(false);
                manager.namePlayer1.SetActive(true);

                manager.chooseCard = true;
                manager.addNum = 1;
                turnPlayer2 = false;
                turnPlayer1 = true;

                break;

            case State.Player2:
                manager.namePlayer1.SetActive(false);
                manager.namePlayer2.SetActive(true);

                turnPlayer1 = false;
                turnPlayer2 = true;

                if (uiButton.Easy)
                {
                    manager.Easy();
                }
                else if (uiButton.Hard)
                {
                    manager.Hard();
                }
                break;

            case State.FinishGame:

                break;
        }
	}
}
