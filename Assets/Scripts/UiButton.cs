﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class UiButton : MonoBehaviour {

    public GameObject easy;
    public GameObject hard;
    public GameObject button;

    public bool Easy;
    public bool Hard;

    public void StartGame()
    {
        SceneManager.LoadScene("GameMap");
        DontDestroyOnLoad(button);
    }

    public void Quit()
    {
        Application.Quit();
    }

    public void ChooseDifficult()
    {
        easy.SetActive(true);
        hard.SetActive(true);
    }

    public void HardDifficult()
    {
        Easy = false;
        Hard = true;
    }


    public void EasyDifficult()
    {
        Easy = true;
        Hard = false;
    }
}
