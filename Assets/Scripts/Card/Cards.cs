﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.U2D;

public class Cards : MonoBehaviour {
 
    public int cardClicked;

    public bool isClicked;

    public int cardValue;

    private bool inizialized = false;

    private Sprite _cardBack;
    private Sprite _cardFace;

    public GameObject card;

    GameManager manager;

    void Start () {
        manager = GameObject.FindGameObjectWithTag("Manager").GetComponent<GameManager>();
        TurnManager.instance.create = true;
        cardClicked = 0;
        isClicked = false;
    }

    public void SetUpGraphic()
    {
        _cardBack = manager.GetCardBack();
        _cardFace = manager.GetCardFace(cardValue);
    }

    public void FlipCard()
    {
        cardClicked = 1;
        if (cardClicked == 0)
        {
            card.GetComponent<Image>().sprite = _cardBack;
        }
        else if (cardClicked == 1)
        {
            card.GetComponent<Image>().sprite = _cardFace;
        }
        SetUpGraphic();
    }

    public int CardValue
    {
        get { return cardValue; }
        set { cardValue = value; }
    }

    public bool Inizialized
    {
        get {return inizialized; }
        set {inizialized = value; }
    }

    public bool IsClicked
    {
        get { return isClicked; }
        set { isClicked = value; }
    }

    public int CardClicked
    {
        get { return cardClicked; }
        set { cardClicked = value; }
    }

    public void ControlCheck()
    {
        StartCoroutine(Pause());
    } 

    IEnumerator Pause()
    {
        yield return new WaitForSeconds(2);
        if (cardClicked == 0)
        {
            card.GetComponent<Image>().sprite = _cardBack;
        }
        else if (cardClicked == 1)
        {
            card.GetComponent<Image>().sprite = _cardFace;
        }
        ChangeTurn();
    }

    public void ChangeTurn()
    {
        if (TurnManager.instance.TurnPlayer)
        {
            if (manager.switchTurn)
            {
                TurnManager.instance.SetState(TurnManager.State.Player2);
                manager.switchTurn = false;
            }
        }
        else if (TurnManager.instance.TurnPlayer2)
        {
            if (manager.switchTurn)
            {
                TurnManager.instance.SetState(TurnManager.State.Player1);
                manager.switchTurn = false;
            }
        }
    }
}
